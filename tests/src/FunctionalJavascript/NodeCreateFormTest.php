<?php

namespace Drupal\Tests\advancedform\FunctionalJavascript;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\filter\Entity\FilterFormat;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\Tests\field\Traits\EntityReferenceTestTrait;
use Drupal\Tests\taxonomy\Traits\TaxonomyTestTrait;

/**
 * Ensure Advancedform can hide a section on the node create form.
 *
 * @group advancedform
 */
class NodeCreateFormTest extends WebDriverTestBase {

  use TaxonomyTestTrait;
  use EntityReferenceTestTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'advancedform',
    'node',
    'taxonomy',
  ];

  /**
   * Theme to test in.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * ID of our test content type.
   *
   * @var string
   */
  protected $bundleId = 'test';

  /**
   * Vocabulary for testing.
   *
   * @var \Drupal\taxonomy\VocabularyInterface
   */
  protected $vocabulary;

  /**
   * Input format for body field.
   *
   * @var \Drupal\filter\FilterFormatInterface
   */
  protected $filteredHtmlFormat;

  /**
   * ID for the term select which triggers show/hide behaviour.
   *
   * @var string
   */
  protected $subtypeFieldId = 'subtype';

  /**
   * Taxonomy term reference field to set edit mode.
   *
   * @var \Drupal\field\FieldConfigInterface
   */
  protected $subtypeField;

  /**
   * ID for the field which is shown/hidden.
   *
   * @var string
   */
  protected $extendedFieldId = 'extended';

  /**
   * Label for the field which is shown/hidden.
   *
   * @var string
   */
  protected $extendedFieldLabel = 'Extended field';

  /**
   * Field which is revealed when in "extended" mode.
   *
   * @var \Drupal\field\FieldConfigInterface
   */
  protected $extendedField;

  /**
   * Term which hides extended fields.
   *
   * @var \Drupal\taxonomy\TermInterface
   */
  protected $standardTerm;

  /**
   * Term which reveals extended fields.
   *
   * @var \Drupal\taxonomy\TermInterface
   */
  protected $extendedTerm;

  /**
   * {@inheritdoc}
   */
  protected function setUp() : void {
    parent::setUp();

    $this->setupContentTypes();

    $user = $this->drupalCreateUser([
      'access content',
      'edit own test content',
      'create test content',
      $this->filteredHtmlFormat->getPermissionName(),
    ]);

    $this->drupalLogin($user);
  }

  /**
   * Tests that fields are hidden via configuration until revealed.
   */
  public function testVisibilityFromToggleButton() {

    // Configure to hide the "Revision log" input.
    \Drupal::configFactory()
      ->getEditable('advancedform.advancedformsettings')
      ->set('rules_global', "#node-test-form:[ .field--name-revision-log ]")
      ->save();

    // Visit "Add a test node".
    $assertSession = $this->assertSession();
    $this->drupalGet("node/add/{$this->bundleId}");

    // Initial state hides "Revision log" description.
    $assertSession->pageTextContains('Switch to advanced');
    $assertSession->pageTextNotContains('Briefly describe the changes you have made.');

    // Reveal "advanced" inputs.
    $this->click('button.advanced-form-toggle');
    $assertSession->pageTextContains('Switch to filtered');
    $assertSession->pageTextContains('Briefly describe the changes you have made.');

    // Hide "advanced" inputs.
    $this->click('button.advanced-form-toggle');
    $assertSession->pageTextContains('Switch to advanced');
    $assertSession->pageTextNotContains('Briefly describe the changes you have made.');
  }

  /**
   * Tests that fields are hidden via configuration until revealed.
   *
   * @todo Set label in setupContentTypes(), check for label not ID here.
   */
  public function testVisibilityFromTermConditionality() {

    // Configure to hide an "extended" input field.
    \Drupal::configFactory()
      ->getEditable('advancedform.advancedformsettings')
      ->set('rules_global', "#node-{$this->bundleId}-form:[.selected-standard .field--name-{$this->extendedFieldId}]")
      ->save();

    // Visit "Add a test node".
    $assertSession = $this->assertSession();
    $this->drupalGet("node/add/{$this->bundleId}");

    // Default subtype value should be set.
    $this->assertSession()->fieldValueEquals($this->subtypeFieldId, $this->standardTerm->id());

    $selectorWhenStandard = "#node-{$this->bundleId}-form.selected-{$this->standardTerm->getName()}";
    $selectorWhenExtended = "#node-{$this->bundleId}-form.selected-{$this->extendedTerm->getName()}";

    // Form has "selected-standard" class and not "selected-extended".
    $this->assertSession()->elementExists('css', $selectorWhenStandard);
    $this->assertSession()->elementNotExists('css', $selectorWhenExtended);
    $assertSession->pageTextNotContains($this->extendedFieldLabel);

    // Select 'extended' subtype; extended fields shown.
    $this->getSession()->getPage()->selectFieldOption($this->subtypeFieldId, $this->extendedTerm->id());
    $this->assertSession()->elementExists('css', $selectorWhenExtended);
    $this->assertSession()->elementNotExists('css', $selectorWhenStandard);
    $assertSession->pageTextContains($this->extendedFieldLabel);

    // Select 'standard' subtype, extended fields hidden again.
    $this->getSession()->getPage()->selectFieldOption($this->subtypeFieldId, $this->extendedTerm->id());
    $this->assertSession()->elementExists('css', $selectorWhenExtended);
    $this->assertSession()->elementNotExists('css', $selectorWhenStandard);
    $assertSession->pageTextContains($this->extendedFieldLabel);
  }

  /**
   * Configure content type and dependencies for testing.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function setupContentTypes() : void {
    $this->filteredHtmlFormat = FilterFormat::create([
      'format' => 'filtered_html',
      'name' => 'Filtered HTML',
    ]);
    $this->filteredHtmlFormat->save();

    $this->drupalCreateContentType(['type' => $this->bundleId]);
    $this->vocabulary = $this->createVocabulary([
      'name' => 'subtype',
      'vid' => 'subtype',
    ]);

    $this->standardTerm = $this->createTerm($this->vocabulary, ['name' => 'standard']);
    $this->extendedTerm = $this->createTerm($this->vocabulary, ['name' => 'extended']);

    FieldStorageConfig::create([
      'field_name' => $this->extendedFieldId,
      'field_label' => $this->extendedFieldLabel,
      'entity_type' => 'node',
      'type' => 'integer',
    ])->save();

    $conditionalFieldConfig = FieldConfig::create([
      'field_name' => $this->extendedFieldId,
      'field_label' => $this->extendedFieldLabel,
      'entity_type' => 'node',
      'bundle' => $this->bundleId,
    ])->save();

    $this->subtypeFieldId = 'taxonomy_' . $this->vocabulary->id();
    $handler_settings = [
      'target_bundles' => [
        $this->vocabulary->id() => $this->vocabulary->id(),
      ],
      'auto_create' => TRUE,
    ];
    $this->createEntityReferenceField('node', $this->bundleId, $this->subtypeFieldId, 'Subtype', 'taxonomy_term', 'default', $handler_settings, 1);
    $this->subtypeField = FieldConfig::loadByName('node', $this->bundleId, $this->subtypeFieldId);
    $this->subtypeField
      ->setDefaultValue($this->standardTerm->id())
      ->save();

    /** @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface $display_repository */
    $display_repository = \Drupal::service('entity_display.repository');
    $display_repository->getFormDisplay('node', $this->bundleId)
      ->setComponent($this->subtypeFieldId, [
        'type' => 'options_select',
      ])
      ->setComponent($this->extendedFieldId, [
        'type' => 'number',
        'settings' => [
          'placeholder' => '0',
        ],
      ])
      ->save();
    $display_repository->getViewDisplay('node', $this->bundleId)
      ->setComponent($this->subtypeFieldId, [
        'type' => 'entity_reference_label',
      ])
      ->setComponent('conditional', [
        'type' => 'number_unformatted',
      ])
      ->save();
    $this->extendedField = FieldConfig::loadByName('node', $this->bundleId, $this->extendedFieldId);
    $this->extendedField
      ->setLabel($this->extendedFieldLabel)
      ->save();
  }

}
