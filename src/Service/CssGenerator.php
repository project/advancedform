<?php

namespace Drupal\advancedform\Service;

/**
 * Generates CSS from Advanced Form rules.
 */
class CssGenerator {

  /**
   * Transform configuration input to CSS rules.
   *
   * @param string $rules
   *   The configured rules as expressed on the Advanced Form config UI.
   *
   * @return string
   *   The generated CSS snippet.
   */
  public function cssFromRules($rules = '') : string {
    $ruleset = explode(PHP_EOL, $rules);
    $selectors = [];
    $css = '';
    foreach ($ruleset as $rule) {
      $selectors[] = strstr($rule, ':', TRUE);
    }
    foreach ($selectors as $key => $selector) {
      $concatSelectors = strstr($ruleset[$key], '[');
      $concatSelectors = str_replace('[', '', $concatSelectors);
      $concatSelectors = str_replace(']', '', $concatSelectors);
      $css .= 'form.advanced-form-filtered';
      $css .= $selector . '' . $concatSelectors . ' {' . PHP_EOL;
      $css .= '  display: none;' . PHP_EOL;
      $css .= '}' . PHP_EOL;
    }
    return $css;
  }

}
