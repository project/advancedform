<?php

namespace Drupal\advancedform\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Settings form for Advancedform.
 */
class AdvancedFormSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'advancedform.advancedformsettings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'advancedform_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('advancedform.advancedformsettings');
    $form['rules_global'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Rules'),
      '#description' => $this->t('Rule selectors to hide inputs, eg <code>node-form:[#revision-information]</code> will hide the revision information fieldset on node forms. One rule per line.'),
      '#default_value' => $config->get('rules_global'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $enteredRules = $form_state->getValue('rules_global');
    $rules = trim($enteredRules);
    if (!empty($rules)) {
      $this->config('advancedform.advancedformsettings')
        ->set('rules_global', $rules)
        ->save();
    }
  }

}
